﻿using FairyGUI;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using XiaWorld;

namespace FRACSTweaks
{
    [HarmonyPatch(typeof(Wnd_JianghuTalk), "JustTalk")]    
    static class Wnd_JianghuTalk_JustTalk
    {
        static void Postfix(Wnd_JianghuTalk __instance, Npc ___target, int ___targetseed)
        {
            Mod.Log.Info?.Write($"PATCH FOR JUSTTALK FIRED for target: {___target?.Name} with seed: {___targetseed}");

            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                // Unlock the person you're talking to           
                KLog.Info($"Unlocking target: {___target.Name}");
                Mod.Log.Info?.Write($"Unlocking target NPC: {___target.Name}");
                UnlockAllNpcData(___targetseed);

                foreach (KeyValuePair<int, JianghuMgr.JHNpcData> kvp in JianghuMgr.Instance.KnowNpcData)
                {
                    if (kvp.Key != ___targetseed)
                    {
                        if (kvp.Key != ___targetseed && JianghuMgr.Instance.CheckNpcInterestOther(___targetseed, kvp.Key))
                        {
                            string npcName = JianghuMgr.Instance.GetJHNpcName(kvp.Key);
                            Mod.Log.Info?.Write($" -- Unlocking NPC: {npcName} with seed: {kvp.Key}");
                            UnlockAllNpcData(kvp.Key);
                        }
                    }
                }
            }
        }

        static void UnlockAllNpcData(int seed)
        {
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.LikeItem, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.LikeItem2, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.LikeItem3, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.LikeItem4, false);

            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.HateItem, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.HateItem2, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.HateItem3, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.HateItem4, false);

            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.Carry, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.Carry2, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.Carry3, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.Carry4, false);
            JianghuMgr.Instance.UnLockNpcDataKnow(seed, g_emJHNpcDataType.Carry5, false);
        }
    }

    [HarmonyPatch(typeof(Wnd_JianghuTalk), "TalkAbout")]
    [HarmonyPatch(new Type[] { typeof(Wnd_JianghuStory.SelectTargetData), typeof(string) })]
    static class Wnd_JianghuTalk_TalkAbout_Data_Word
    {
        static void Postfix(Wnd_JianghuTalk __instance, Wnd_JianghuStory.SelectTargetData data, string word)
        {
            Mod.Log.Info?.Write($"PATCH FOR TALKABOUT(data, word) FIRED.");
        }
    }

    [HarmonyPatch(typeof(Wnd_JianghuTalk), "TalkAbout")]
    [HarmonyPatch(new Type[] {})]
    static class Wnd_JianghuTalk_TalkAbout_NoParam
    {
        static void Postfix(Wnd_JianghuTalk __instance)
        {
            Mod.Log.Info?.Write($"PATCH FOR TALKABOUT() FIRED.");
        }
    }


    [HarmonyPatch(typeof(Wnd_NpcInfo), "OnInit")]
    static class Wnd_NpcInfo_OnInit
    {
        static void Postfix(Wnd_NpcInfo __instance, Panel_NpcRelation ___RelationPanel)
        {
            Mod.Log.Info?.Write("W_NPCI:OI- entered.");

            __instance.UIInfo.m_NpcInfo.onClick.Add(delegate ()
            {
                if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
                {
                    Mod.Log.Info?.Write($"Cheat code click detected for NPC: {__instance?.npc?.Name}");
                    if ((bool)(__instance?.npc?.IsPlayerThing) && !__instance.npc.IsDisciple)
                    {
                        Mod.Log.Info?.Write($" Character is a valid outer disciple");

                        // 20 is max skill, love of 1 = passionate, 2 = obsessed
                        Mod.Log.Info?.Write($" -- SKILL DATA --");
                        var fight = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Fight);
                        var qiSense = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Qi);
                        var socialContact = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.SocialContact);
                        var medicine = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Medicine);
                        var cooking = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Cooking);
                        var building = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Building);
                        var farming = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Farming);
                        var mining = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Mining);
                        var art = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Art);
                        var manual = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Manual);

                        var danQi = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.DanQi);  // magic crafting
                        var douFa = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.DouFa); // magic
                        var fabao = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Fabao);
                        var zhen = __instance.npc.PropertyMgr.SkillData.GetSkill(g_emNpcSkillType.Zhen);

                        Mod.Log.Info?.Write($" -- Fight => base: {fight.BaseLevel}  addV: {fight.Addv}  addv2: {fight.Addv2}  love: {fight.Love}  ban: {fight.Ban}");
                        Mod.Log.Info?.Write($" -- qiSense => base: {qiSense.BaseLevel}  addV: {qiSense.Addv}  addv2: {qiSense.Addv2}  love: {qiSense.Love}  ban: {qiSense.Ban}");
                        Mod.Log.Info?.Write($" -- socialContact => base: {socialContact.BaseLevel}  addV: {socialContact.Addv}  addv2: {socialContact.Addv2}  love: {socialContact.Love}  ban: {socialContact.Ban}");
                        Mod.Log.Info?.Write($" -- medicine => base: {medicine.BaseLevel}  addV: {medicine.Addv}  addv2: {medicine.Addv2}  love: {medicine.Love}  ban: {medicine.Ban}");
                        Mod.Log.Info?.Write($" -- cooking => base: {cooking.BaseLevel}  addV: {cooking.Addv}  addv2: {cooking.Addv2}  love: {cooking.Love}  ban: {cooking.Ban}");
                        Mod.Log.Info?.Write($" -- building => base: {building.BaseLevel}  addV: {building.Addv}  addv2: {building.Addv2}  love: {building.Love}  ban: {building.Ban}");
                        Mod.Log.Info?.Write($" -- farming => base: {farming.BaseLevel}  addV: {farming.Addv}  addv2: {farming.Addv2}  love: {farming.Love}  ban: {farming.Ban}");
                        Mod.Log.Info?.Write($" -- mining => base: {mining.BaseLevel}  addV: {mining.Addv}  addv2: {mining.Addv2}  love: {mining.Love}  ban: {mining.Ban}");
                        Mod.Log.Info?.Write($" -- art => base: {art.BaseLevel}  addV: {art.Addv}  addv2: {art.Addv2}  love: {art.Love}  ban: {art.Ban}");
                        Mod.Log.Info?.Write($" -- manual => base: {manual.BaseLevel}  addV: {manual.Addv}  addv2: {manual.Addv2}  love: {manual.Love}  ban: {manual.Ban}");

                        Mod.Log.Info?.Write($" -- danQi => base: {danQi.BaseLevel}  addV: {danQi.Addv}  addv2: {danQi.Addv2}  love: {danQi.Love}  ban: {danQi.Ban}");
                        Mod.Log.Info?.Write($" -- douFa => base: {douFa.BaseLevel}  addV: {douFa.Addv}  addv2: {douFa.Addv2}  love: {douFa.Love}  ban: {douFa.Ban}");
                        Mod.Log.Info?.Write($" -- fabao => base: {fabao.BaseLevel}  addV: {fabao.Addv}  addv2: {fabao.Addv2}  love: {fabao.Love}  ban: {fabao.Ban}");
                        Mod.Log.Info?.Write($" -- zhen => base: {zhen.BaseLevel}  addV: {zhen.Addv}  addv2: {zhen.Addv2}  love: {zhen.Love}  ban: {zhen.Ban}");

                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Fight, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Qi, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.SocialContact, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Medicine, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Cooking, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Building, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Farming, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Mining, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Art, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.Manual, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.DanQi, 20, false);
                        __instance.npc.PropertyMgr.SkillData.AddSkillBaseLevel(g_emNpcSkillType.DouFa, 20, false);

                        Mod.Log.Info?.Write($" -- BASE DATA --");
                        var perception = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Perception, null);
                        var physique = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Physique, null);
                        var charisma = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Charisma, null);
                        var intelligence = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Intelligence, null);
                        var luck = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Luck, null);
                        var move = __instance.npc.PropertyMgr.BaseData.GetValue(g_emNpcBasePropertyType.Movement, null);

                        Mod.Log.Info?.Write($" -- BEFORE => PER: {perception}  PHY: {physique}  CHA: {charisma}  INT: {intelligence}  LUK: {luck}  MOV: {move}");

                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Perception, 20f);
                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Physique, 20f);
                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Charisma, 20f);
                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Intelligence, 20f);
                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Luck, 20f);

                        __instance.npc.PropertyMgr.BaseData.SetBaseValue(g_emNpcBasePropertyType.Movement, 1f);
                        __instance.npc.PropertyMgr.ModifierProperty("MoveSpeed", 4, 0f, 0f, 0f);
                    }
                }
            }
            );
        }

    }

}
