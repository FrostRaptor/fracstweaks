﻿using System;
using System.IO;

namespace FRACSTweaks.Logging
{
    // Basic logger that does not evaluate log messages until the specified level is enabled.
    //  More performant that other loggers due to the short-circuited evaluation, which prevents
    //  string creation (and garbage). 
    public class DeferringLogger
    {
        private readonly string LogFile;

        private readonly StreamWriter LogStream;
        public readonly string LogLabel;

        public readonly bool IsDebug;
        public readonly bool IsTrace;

        readonly ModLogWriter ModOnlyWriter;
        readonly ModLogWriter CombinedWriter;

        public DeferringLogger(string modDir, string logFilename = "mod", bool isDebug = false, bool isTrace = false) : this(modDir, logFilename, "mod", isDebug, isTrace)
        {
        }

        public DeferringLogger(string modDir, string logFilename = "mod", string logLabel = "mod", bool isDebug = false, bool isTrace = false)
        {
            if (LogFile == null)
            {
                LogFile = Path.Combine(modDir, $"{logFilename}.log");
            }

            if (File.Exists(LogFile))
            {
                File.Delete(LogFile);
            }

            LogStream = File.AppendText(LogFile);
            LogLabel = "<" + logLabel + ">";

            IsDebug = isDebug;
            IsTrace = isTrace;

            ModOnlyWriter = new ModLogWriter(LogStream);
            CombinedWriter = new ModLogWriter(LogStream);
        }

        public Nullable<ModLogWriter> Trace
        {
            get { return IsTrace ? (Nullable<ModLogWriter>)ModOnlyWriter : null; }
            private set { }
        }

        public Nullable<ModLogWriter> Debug
        {
            get { return IsDebug ? (Nullable<ModLogWriter>)ModOnlyWriter : null; }
            private set { }
        }

        public Nullable<ModLogWriter> Info
        {
            get { return (Nullable<ModLogWriter>)ModOnlyWriter; }
            private set { }
        }

    }
    
}

