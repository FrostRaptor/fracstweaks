﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FRACSTweaks.Logging
{

    public struct ModLogWriter
    {
        readonly StreamWriter LogStream;

        public ModLogWriter(StreamWriter sw)
        {
            LogStream = sw;        
        }

        public void Write(string message)
        {
            // Write our internal log
            string now = DateTime.UtcNow.ToString("HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
            LogStream.WriteLine(now + " " + message);
            LogStream.Flush();
        }

        public void Write(Exception e, string message = null)
        {
            // Write our internal log
            string now = DateTime.UtcNow.ToString("HH:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture);
            if (message != null) LogStream.WriteLine(now + " " + message);
            LogStream.WriteLine(now + " " + e?.Message);
            LogStream.WriteLine(now + " " + e?.StackTrace);

            if (e?.InnerException != null)
            {
                LogStream.WriteLine(now + " " + e?.InnerException.Message);
                LogStream.WriteLine(now + " " + e?.InnerException.StackTrace);

                if (e.InnerException?.InnerException != null)
                {
                    LogStream.WriteLine(now + " " + e?.InnerException.InnerException.Message);
                    LogStream.WriteLine(now + " " + e?.InnerException.InnerException.StackTrace);
                }
            }

            LogStream.Flush();         
        }
    }
}
