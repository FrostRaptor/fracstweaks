﻿using FRACSTweaks.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FRACSTweaks
{
    // MLL looks for a main class name that matches the assembly name. 
    public static class FRACSTweaks
    {
        public static void OnInit()
        {
            Mod.OnInit();
        }
    }

    public static class Mod
    {
        public const string HarmonyPackage = "us.frostraptor.ACS.Tweaks";
        public const string LogName = "FRACS";
        public static DeferringLogger Log;

        public static void OnInit()
        {
            string modDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Log = new DeferringLogger(modDirectory, LogName, null, true, true);
            Log.Info?.Write($"Mod: {HarmonyPackage} initialized.");

            KLog.Info("[FRACS] OnInit() invoked!", new object[0]);
            KLog.Info($"  modDir is: {modDirectory}", new object[0]);
        }
    }
}
